#!/bin/bash
APP="NZBmegasearch"
REPO="https://github.com/Mirabis/usntssearch.git"
INSTALLDIR="/opt"

if [[ -f /var/lib/installed ]]; then
  echo "Updating source..."
  mkdir -p ${INSTALLDIR} && cd ${INSTALLDIR}
  git pull
else
  echo "Downloading source..."
  tmpdir="/tmp/tmp.$(( $RANDOM * $RANDOM * $RANDOM * $RANDOM ))"
  git clone --depth=1 ${REPO} ${tmpdir}
  if [[ $? -eq 0 ]]; then
    rm -rf ${INSTALLDIR}
    mv ${tmpdir} ${INSTALLDIR}
    touch /var/lib/installed
  else
    rm -rf ${tmpdir}
  fi
fi
# Fix permissions
chown -R nobody:users ${INSTALLDIR}

# megasearch doesn't have a handy config file command line argument
ln -fs /config/custom_params.ini /opt/NZBmegasearch/custom_params.ini
